/*
 * Copyright (c) 2009-2010, 2013, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of The Linux Foundation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef BATTERY_MONITOR_H
#define BATTERY_MONITOR_H

#define LOG_TAG "QC-BMS"

/* bms paths */
#define PERSISTENT_LOCATION	"/data/bms/"
#define BATTERY_POWER_SUPPLY	"/sys/class/power_supply/battery/"
#define BMS_SYS_MODULE_PM8XXX	"/sys/module/pm8921_bms/parameters/"
#define BMS_SYS_MODULE_QPNP	"/sys/module/qpnp_bms/parameters/"
#define SYS_BMS			"/sys/module/pm8921_bms/parameters/"
#define BMS_SYS_PM8XXX		"/sys/bus/platform/devices/pm8921-bms/"
#define BMS_SYS_QPNP		"/sys/bus/spmi/devices/"
/* bms module parameters */
#define LAST_CHARGE_INCREASE	"last_charge_increase"
#define LAST_CHARGE_CYCLES	"last_chargecycles"
#define LAST_OCV		"last_ocv_uv"
#define LAST_RBATT		"last_rbatt"
#define PID_FILENAME		"daemon_pid"
#define BATTERY_REMOVED		"battery_removed"
#define LAST_FCC_UPDATE_COUNT	"last_fcc_update_count"
#define MIN_FCC_CYCLES		"min_fcc_cycles"
#define FCC_DATA_TMP		"tmp_data"
/* battery power_supply class */
#define BATTERY_PS_TEMP		"temp"
#define BATTERY_PS_CYCLE_COUNT	"cycle_count"
/* fcc learning files */
#define BMS_FCC			"fcc_data"
#define BMS_FCC_TEMP		"fcc_temp"
#define BMS_FCC_CHGCYL		"fcc_chgcyl"
/* bms constants */
#define MAX_FCC_ENTRIES		10

struct fcc_learning_table {
	int fcc_new;
	int chargecycles;
	int batt_temp;
};

int directory_exists(char *path);
int file_exists(char *path);
int open_file(char *prefix, char *path, int flags);
int read_int_from_file(char *prefix, char *path, int *ret_val);
int write_string_to_file(char *prefix, char *path, char *string);
int write_int_to_file(char *prefix, char *path, int value);
int rename_file(char *oldprefix, char *oldpath, char *newprefix, char *newpath);
#endif  /* BATTERY_MONITOR_H */
