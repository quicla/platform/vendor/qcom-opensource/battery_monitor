/*
 * Copyright (c) 2009-2010, 2013, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of The Linux Foundation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <linux/rtc.h>
#include <linux/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include <sys/poll.h>
#include <cutils/log.h>
#include "battery_monitor.h"

#define KERNEL_EINVAL -22

static struct fcc_learning_table *fcc_table;
static char *bms_sys, *bms_module_sys;
static int prev_fcc_count = -EINVAL;
static pthread_mutex_t read_mutex;

void write_persisted_values()
{
	int value;
	int rc;
	DIR *dp;
	struct dirent *dirp;


	if ((dp = opendir(bms_module_sys)) == NULL)
		/* can't read directory */
		return;

	while ((dirp = readdir(dp)) != NULL) {
		/* ignore dot and dot-dot */
		if (strcmp(dirp->d_name, ".") == 0 ||
			strcmp(dirp->d_name, "..") == 0)
			continue;

		value = 0;
		rc = read_int_from_file(PERSISTENT_LOCATION, dirp->d_name, &value);
		if (rc == 0 && value != KERNEL_EINVAL)
			write_int_to_file(bms_module_sys, dirp->d_name, value);

	}
	closedir(dp);
}

void read_values_and_store(int clear_data)
{
	int value;
	int rc;
	DIR *dp;
	struct dirent *dirp;

	if ((dp = opendir(bms_module_sys)) == NULL)
		/* can't read directory */
		return;

	pthread_mutex_lock(&read_mutex);

	while ((dirp = readdir(dp)) != NULL) {
		/* ignore dot and dot-dot */
		if (strcmp(dirp->d_name, ".") == 0 ||
			strcmp(dirp->d_name, "..") == 0)
			continue;

		value = 0;
		rc = read_int_from_file(bms_module_sys, dirp->d_name, &value);
		if ((rc == 0 && value != KERNEL_EINVAL) || clear_data)
			write_int_to_file(PERSISTENT_LOCATION, dirp->d_name, value);

	}
	closedir(dp);

	pthread_mutex_unlock(&read_mutex);
}

void sigaction_handler(int signum, siginfo_t *info, void *context)
{
	read_values_and_store(0);
}

void register_signals(void) {
	struct sigaction act;

	act.sa_sigaction = sigaction_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_SIGINFO;
	act.sa_restorer = 0;
	sigaction(SIGUSR1, &act, 0);
	sigaction(SIGUSR2, &act, 0);
	sigaction(SIGFPE, &act, 0);
}

static void fcc_update_disk(int count)
{
	int fd_data, i, rc = 0;
	struct fcc_learning_table *ft;

	ALOGD("Writing the fcc table to disk!");
	/* Truncate if there is a already existing file */
	fd_data = open_file(PERSISTENT_LOCATION, FCC_DATA_TMP,
						O_TRUNC|O_RDWR|O_CREAT);
	if (fd_data < 0) {
		ALOGE("Unable to open file for re-update");
		return;
	}

	/* Store the fcc table on the disk */
	for (i = 0; i < count; i++) {
		ft = &fcc_table[i];
		ALOGD("Writing [%d], fcc=%d, cc=%d, temp=%d",
			i, ft->fcc_new, ft->chargecycles, ft->batt_temp);
		rc = write(fd_data, ft,	sizeof(*ft));
		if (rc < 0) {
			ALOGE("Unable to write data to disk");
			break;
		}
	}
	close(fd_data);
	if (rc >= 0) {
		rename_file(PERSISTENT_LOCATION, FCC_DATA_TMP,
					PERSISTENT_LOCATION, BMS_FCC);
		ALOGD("Writing fcc table to disk completed");
	} else {
		remove(PERSISTENT_LOCATION FCC_DATA_TMP);
	}
}

static void battery_fcc_read(int fd_sys, int clear)
{
	int rc = 0, fcc_new = 0, batt_temp = 0, chargecycles = 0;
	int fcc_count = 0, fcc_min_cycles = 0, count;
	struct fcc_learning_table *ft;
	char buff[20];

	ALOGD("Write to fcc table triggered!, clear=%d", clear);

	lseek(fd_sys, 0, SEEK_SET);

	if (clear) {
		int fd_data;
		/* dummy read to clear all data */
		read(fd_sys, &buff, 20);
		/* truncate the fcc data file */
		fd_data = open_file(PERSISTENT_LOCATION, BMS_FCC, O_TRUNC);
		if (fd_data < 0) {
			ALOGE("Unable to truncate file '%s'",
					PERSISTENT_LOCATION BMS_FCC);
			return;
		}
		close(fd_data);
		return;
	}

	/* read new FCC */
	rc = read(fd_sys, buff, 20);
	if (rc <= 0) {
		ALOGE("Unable to read new-fcc");
		return;
	}
	fcc_new = strtol(buff, NULL, 10);
	if (fcc_new <= 0) {
		ALOGE("Invalid FCC new");
		return;
	}

	/* read last_fcc_update count */
	rc = read_int_from_file(bms_module_sys, LAST_FCC_UPDATE_COUNT,
							&fcc_count);
	if (rc < 0 || fcc_count < 0) {
		ALOGE("Unable to read FCC update count");
		return;
	}

	/* check if this is new data, else return */
	if (prev_fcc_count == -EINVAL)
		prev_fcc_count = fcc_count;
	else if (prev_fcc_count == fcc_count) {
		ALOGE("No new data, false trigger, ignoring data!");
		return;
	} else
		prev_fcc_count = fcc_count;


	/* read battery temperature */
	rc = read_int_from_file(BATTERY_POWER_SUPPLY,
				BATTERY_PS_TEMP, &batt_temp);
	if (rc < 0) {
		ALOGE("Unable to open Battery power-supply");
		return;
	}

	/* read last_charge_cycle */
	rc = read_int_from_file(bms_module_sys, LAST_CHARGE_CYCLES,
						&chargecycles);
	if (rc < 0 || chargecycles < 0) {
		rc = read_int_from_file(BATTERY_POWER_SUPPLY,
					BATTERY_PS_CYCLE_COUNT, &chargecycles);
		if (rc < 0 || chargecycles < 0) {
			ALOGE("Unable to read charge-cycles");
			return;
		}
	}

	/* read max fcc cycles */
	rc = read_int_from_file(bms_module_sys, MIN_FCC_CYCLES,
							&fcc_min_cycles);
	if (rc < 0 || fcc_min_cycles <= 0) {
		ALOGE("Unable to read max FCC cycles");
		return;
	}

	count = (fcc_count - 1) % fcc_min_cycles;
	fcc_table[count].fcc_new = fcc_new;
	fcc_table[count].batt_temp = batt_temp;
	fcc_table[count].chargecycles = chargecycles;

	ALOGE("New Data!. FCC=%d, temp=%d, chargecycle=%d, fcc_count=%d, min_cycles=%d, location_updated=%d",
		fcc_new, batt_temp, chargecycles, fcc_count, fcc_min_cycles, count);

	count = (fcc_count < fcc_min_cycles) ? fcc_count : fcc_min_cycles;

	/* write back the table to the disk */
	fcc_update_disk(count);
}

static int set_bms_path(void)
{
	int rc, len;
	DIR *dp;
	struct dirent *dirp;

	rc = file_exists(BMS_SYS_PM8XXX BMS_FCC);
	if (!rc) {
		if ((dp = opendir(BMS_SYS_QPNP)) == NULL)
			return -ENODEV;

		while ((dirp = readdir(dp)) != NULL) {
			if (strstr(dirp->d_name, "qpnp-bms"))
				break;
		}
		if (dirp == NULL) {
			ALOGE("Unable to locate BMS node\n");
			return -ENODEV;
		}

		len = strlen(BMS_SYS_QPNP) + strlen(dirp->d_name) +
						strlen(BMS_FCC) + 2;
		bms_sys = malloc(len);
		if (!bms_sys) {
			ALOGE("Unable to allocate memory");
			return -ENOMEM;
		}
		snprintf(bms_sys, len, "%s%s/%s", BMS_SYS_QPNP,
						dirp->d_name, BMS_FCC);
		rc = file_exists(bms_sys);
		if (!rc) {
			ALOGE("FCC learning not enabled!");
			goto free_bms_sys;
		}
		snprintf(bms_sys, len, "%s%s/", BMS_SYS_QPNP, dirp->d_name);
	} else {
		bms_sys = malloc(strlen(BMS_SYS_PM8XXX) + 1);
		if (!bms_sys) {
			ALOGE("Unable to allocate memory");
			return -ENOMEM;
		}
		memcpy(bms_sys, BMS_SYS_PM8XXX, strlen(BMS_SYS_PM8XXX) + 1);
	}
	ALOGD("Set 'bms_sys' = %s", bms_sys);

	/* set the module parameter directory */
	rc = directory_exists(BMS_SYS_MODULE_PM8XXX);
	if (!rc) {
		rc = directory_exists(BMS_SYS_MODULE_QPNP);
		if (!rc) {
			ALOGE("BMS module params do not exist");
			rc = -ENODEV;
			goto free_bms_sys;
		}
		bms_module_sys = malloc(strlen(BMS_SYS_MODULE_QPNP) + 1);
		if (!bms_module_sys) {
			ALOGE("Unable to allocate memory");
			rc = -ENOMEM;
			goto free_bms_sys;
		}
		memcpy(bms_module_sys, BMS_SYS_MODULE_QPNP,
					strlen(BMS_SYS_MODULE_QPNP) + 1);
	} else {
		bms_module_sys = malloc(strlen(BMS_SYS_MODULE_PM8XXX) + 1);
		if (!bms_module_sys) {
			ALOGE("Unable to allocate memory");
			rc = -ENOMEM;
			goto free_bms_sys;
		}
		memcpy(bms_module_sys, BMS_SYS_MODULE_PM8XXX,
					strlen(BMS_SYS_MODULE_PM8XXX) + 1);
	}

	ALOGD("Set 'bms_module_sys' = %s", bms_module_sys);

	return 0;

free_bms_sys:
	free(bms_sys);
fail_open:
	return rc;
}

static void fcc_learning(void *recv_arg)
{
	struct pollfd pfd;
	int fd_sys, fd_data, i = 0, rc = 0, count;
	int batt_removed = 0, fcc_count, dummy = 0, fcc_min_cycles;
	struct fcc_learning_table *ft;
	char *fcc_path;

	fd_sys = open_file(bms_sys, BMS_FCC, O_RDWR);
	if (fd_sys < 0) {
		ALOGE("Unable to open FCC file");
		goto fail_fcc;
	}

	/* read the FCC data from exisitng file, create file if not present */
	fd_data = open_file(PERSISTENT_LOCATION, BMS_FCC, O_RDONLY);
	if (fd_data < 0) {
		ALOGE("Unable to open file '%s', Creating it",
					PERSISTENT_LOCATION BMS_FCC);
		fd_data = open_file(PERSISTENT_LOCATION, BMS_FCC,
						O_CREAT|O_RDWR|O_TRUNC);
		if (fd_data < 0) {
			ALOGE("Unable to create file '%s', Quiting!",
						PERSISTENT_LOCATION BMS_FCC);
			goto fail_fcc_data;
		}
	}

	/* check if battery data is valid */
	rc = read_int_from_file(bms_module_sys, BATTERY_REMOVED, &batt_removed);
	if (batt_removed <= 0)
		batt_removed = 0;

	/* if battery has been removed, update parameters and table */
	if (batt_removed) {
		ALOGE("Battery has been removed. Reset data");
		battery_fcc_read(fd_sys, 1);
		read_values_and_store(1);
	}

	/* create the FCC table, size = max fcc cycles */
	rc = read_int_from_file(bms_module_sys, MIN_FCC_CYCLES,	&fcc_min_cycles);
	if (rc < 0 || fcc_min_cycles < 0 || fcc_min_cycles > MAX_FCC_ENTRIES) {
		ALOGE("Invalid Min FCC cycles, setting %d", MAX_FCC_ENTRIES);
		fcc_min_cycles = MAX_FCC_ENTRIES;
	}
	fcc_table = malloc(sizeof(*fcc_table) * (fcc_min_cycles + 1));
	if (!fcc_table) {
		ALOGE("Unable to allocate memory to fcc_table");
		goto fail_malloc;
	}

	ALOGD("Reading fcc data from the disk-file");
	i = 0;
	while (read(fd_data, &fcc_table[i], sizeof(*fcc_table)) > 0) {
		ALOGD("Data Read [%d], fcc = %d, cc = %d, batt_temp = %d",
			i, fcc_table[i].fcc_new, fcc_table[i].chargecycles,
						fcc_table[i].batt_temp);
		i++;
		if (i == fcc_min_cycles) {
			ALOGD("Max table size reached");
			break;
		}
	}
	close(fd_data);

	fcc_count = i;

	ALOGD("FCC data read from disk. Total entries=%d", fcc_count);
	/* write back data to sysfs */
	if (fcc_count)
		for (i = 0; i < fcc_count; i++) {
			ft = &fcc_table[i];
			write_int_to_file(bms_sys, BMS_FCC, ft->fcc_new);
			write_int_to_file(bms_sys, BMS_FCC_TEMP, ft->batt_temp);
			write_int_to_file(bms_sys, BMS_FCC_CHGCYL, ft->chargecycles);
		}


	/* set the poll configuration */
	pfd.fd = fd_sys;
	pfd.events = POLLERR|POLLPRI;
	pfd.revents = 0;

	/* dummy read */
	read(fd_sys, &dummy, sizeof(dummy));

	while (1) {
		int pollres;
		pollres = poll(&pfd, 1, -1);
		if (pollres < 0) {
			ALOGE("pollres  poll failed");
			goto fail_poll;
		}
		if (pfd.revents & (POLLERR|POLLPRI)) {
			/* read and store the other paramters */
			read_values_and_store(0);
			/* read and store the learnt fcc values */
			battery_fcc_read(pfd.fd, 0);
		}
	}

fail_poll:
	free(fcc_table);
fail_malloc:
	close(fd_data);
fail_fcc_data:
	close(fd_sys);
fail_fcc:
	pthread_detach(pthread_self());
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	int last_charge_increase;
	int last_charge_cycles;
	int last_ocv;
	int last_rbatt;
	int rc;
	pid_t mypid;
	pthread_t fcc_learning_thread;

	rc = set_bms_path();
	if (rc < 0) {
		ALOGE("Unable to set BMS paths");
		return rc;
	}

	/* write the pid of this daemon in the killer process */
	mypid = getpid();
	rc = write_int_to_file(PERSISTENT_LOCATION, PID_FILENAME, mypid);
	if (rc < 0) {
		ALOGE("Write to persistent location failed");
		return rc;
	}

	if (pthread_mutex_init(&read_mutex, NULL)) {
		ALOGE("pthead_mutex_init failed");
		return -EINVAL;
	}

	register_signals();

	write_persisted_values();

	/* create a thread which monitors fcc updates */
	rc = pthread_create(&fcc_learning_thread, NULL,
					(void *)&fcc_learning, NULL);
	if (rc) {
		ALOGE("Unable to create thread for fcc-learning");
		return rc;
	}

	while(1)
	{
		read_values_and_store(0);
		/* sleep 30 minutes */
		sleep(30*60);
	}

}

